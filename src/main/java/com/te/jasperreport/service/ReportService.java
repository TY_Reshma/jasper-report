package com.te.jasperreport.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.te.jasperreport.entity.Employee;
import com.te.jasperreport.repository.EmployeeRepository;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class ReportService {

	@Autowired
	DataSource datasource;
	@Autowired
	private EmployeeRepository employeeRepository;

	public String generateReport(String reportFormat) throws FileNotFoundException, JRException, SQLException {
		String path="E:\\jasper-report\\Report";
		List<Employee> listOfEmployees = employeeRepository.findAll();
		//to load a file and complile it
		File file = ResourceUtils.getFile("classpath:employee.jrxml");
		//compile this file
		JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
		//map employees to report
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(listOfEmployees);
		
		Map<String,Object> parameters = new HashMap<>();
		parameters.put("createdBy", "Reshma Satpute");
		 JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,dataSource );
		 if (reportFormat.equalsIgnoreCase("html")) {
			 JasperExportManager.exportReportToHtmlFile(jasperPrint,path+"\\employees.html");
	
		}
		 
		 if (reportFormat.equalsIgnoreCase("pdf")) {
			 JasperExportManager.exportReportToHtmlFile(jasperPrint,path+"\\employees.pdf");
	
		}
		 
		return "report generated in path : "+path;

	}
}
