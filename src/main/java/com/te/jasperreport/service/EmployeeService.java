package com.te.jasperreport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.jasperreport.entity.Employee;
import com.te.jasperreport.repository.EmployeeRepository;

@Service
public class EmployeeService {
	@Autowired
	private EmployeeRepository employeeRepository;

	public List<Employee> getAllEmployee(){
		return employeeRepository.findAll();	
	}
}
