package com.te.jasperreport.controller;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.te.jasperreport.entity.Employee;
import com.te.jasperreport.service.EmployeeService;
import com.te.jasperreport.service.ReportService;

import net.sf.jasperreports.engine.JRException;

@RestController
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private ReportService reportService;

	@GetMapping("/getAllEmployee")
	public List<Employee> getAllEmployee() {
		return employeeService.getAllEmployee();
	}

	@GetMapping("/generateReport/{format}")
	public String generateReport(@PathVariable String format) throws FileNotFoundException, JRException, SQLException {
		return reportService.generateReport(format);

	}

}
